import { v4 as uuid } from 'uuid';

export class DestinoViaje {
  private selected: boolean;
  public servicio: string[];
  id = uuid();

  constructor(
    public nombre: string,
    public descripcion: string,
    public votes: number = 0
  ){
    this.servicio = ['pileta', 'desayuno'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  // tslint:disable-next-line:typedef
  setSelected(s: boolean) {
    this.selected = s;
  }

  voteUp() {
    this.votes++;
  }

  voteDowm() {
    this.votes--;
  }
}
