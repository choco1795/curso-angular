import { Component, OnInit } from '@angular/core';
import {ReservasApiClientService} from '../reservas-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {

  lista;

  constructor(
    private api: ReservasApiClientService
  ) {
    this.lista = api.getAll();
  }

  ngOnInit(): void {
  }

}
