import {Component, EventEmitter, forwardRef, Inject, InjectionToken, OnInit, Output} from '@angular/core';
import {DestinoViaje} from '../../models/destino-viaje.models';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {fromEvent} from 'rxjs';
import {map, filter, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
import {APP_CONFIG, AppConfig} from '../../app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(
    fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['',
        Validators.compose([
          this.nombreValidator,
          this.nombreValidatorParametrizado(this.minLongitud)
        ])
      ],
      descripcion: ['',
        Validators.compose([
          Validators.required,
          this.descripcionValidator
        ])
      ]
    });

    this.fg.valueChanges.subscribe((form: any) => {
      // console.log('cambio el formulario:', form);
    });
  }

  ngOnInit(): void {
    let elementoNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elementoNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 4),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, descripcion: string): boolean {
    const d = new DestinoViaje(nombre, descripcion);
    this.onItemAdded.emit(d);
    return false;
  }

  // @ts-ignore
  nombreValidator(control: FormControl): {[s: string]: boolean} {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 3) {
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizado(minLong: number): ValidatorFn {
    return (control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return {minLongNombre: true};
      }
      return null;
    };
  }

  descripcionValidator(control: FormControl): {[s: string]: boolean} {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {invalidDescripcion: true};
    }
    return null;
  }

}
